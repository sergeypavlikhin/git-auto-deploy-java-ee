CREATE DATABASE IF NOT EXISTS `deploy`;

--Change the username and password to whatever you like. Be sure to make
--the same changes in db.properties
GRANT ALL ON `deploy`.* to 'deploy_user'@'localhost' identified by 'pw123123';

CREATE TABLE `deploy`.`log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payload` varchar(5000) DEFAULT NULL,
  `isSuccess` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `deploy`.`repositories` (
  `slug` varchar(100) NOT NULL DEFAULT '',
  `branch` varchar(100) NOT NULL DEFAULT '',
  `author` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`slug`,`branch`,`author`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;